//
//  addTaskViewController.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 10.04.2021.
//

import UIKit

class addTaskViewController: UIViewController {
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var deadlineTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: addingTaskDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func saveTask(_ sender: Any) {
        let task = Task()
        
        if (taskTextField.text != "") && (deadlineTextField.text != "") {
            task.taskName = taskTextField.text ?? ""
            task.taskDeadline = deadlineTextField.text ?? ""
            
            delegate?.addingTask(task)
            
            dismiss(animated: true, completion: nil)
        } else {
            saveButton.isEnabled = false
            errorLabel.isHidden = false
            errorLabel.text = "Введите данные"
        }
    }
    
    @IBAction func taskEdit(_ sender: Any) {
        saveButton.isEnabled = true
        errorLabel.isHidden = true
    }

}
