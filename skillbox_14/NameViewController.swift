//
//  NameViewController.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 10.04.2021.
//

import UIKit

class NameViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = Persistance.shared.name
        surnameLabel.text = Persistance.shared.surname
    }
    
    @IBAction func nameEdit(_ sender: Any) {
        Persistance.shared.name = nameTextField.text
    }
    
    @IBAction func surnameEdit(_ sender: Any) {
        Persistance.shared.surname = surnameTextField.text
    }
    
}
