//
//  CoreDataToDoViewController.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 10.04.2021.
//

import UIKit

class CoreDataToDoViewController: UIViewController {

    @IBOutlet weak var doListTableView: UITableView!
    
    var taskArray: [TaskCore] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Persistance.shared.loadFromCoredata(&taskArray)
        taskArray = taskArray.sorted { $0.taskName! < $1.taskName! }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? addTaskViewController, segue.identifier  == "addTask" {
            vc.delegate = self
        }
    }

}

extension CoreDataToDoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath)
        let task = taskArray[indexPath.row]
        
        cell.textLabel?.text = task.taskName
        cell.detailTextLabel?.text = task.taskDeadline
        
        return cell
    }
    
}

extension CoreDataToDoViewController: addingTaskDelegate{
    func addingTask(_ task: Task) {
        Persistance.shared.saveToCoredata(task.taskName,task.taskDeadline, &taskArray)
        
        doListTableView.reloadData()
    }
}

