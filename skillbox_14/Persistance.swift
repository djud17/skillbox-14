//
//  Persistance.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 10.04.2021.
//

import Foundation
import RealmSwift
import CoreData

class Task: Object {
    @objc dynamic var taskName = ""
    @objc dynamic var taskDeadline = ""
}

class Persistance {
    
    static let shared = Persistance()
    
    // MARK: - UserDefaults
    
    private let kNameKey = "Persistance.kNameKey"
    private let kSurnameKey = "Persistance.kSurnameKey"
    
    var name: String? {
        set { UserDefaults.standard.set(newValue, forKey: kNameKey)}
        get { return UserDefaults.standard.string(forKey: kNameKey)}
    }
    
    var surname: String? {
        set { UserDefaults.standard.set(newValue, forKey: kSurnameKey)}
        get { return UserDefaults.standard.string(forKey: kSurnameKey)}
    }
    
    // MARK: - Realm
    let realm = try! Realm()
    
    func realmWrite(_ task: Task){
        try! realm.write{
            realm.add(task)
        }
    }
    
    func realmRead(_ taskArray: inout [Task]){
        
        let array = realm.objects(Task.self)
        
        for el in array {
            taskArray.append(el)
        }
    }
    
    // MARK: - Coredata
   
    func saveToCoredata(_ taskName: String,_ taskDeadline: String,_ taskArray: inout [TaskCore]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "TaskCore", in: context) else {return}
        
        let taskObject = TaskCore(entity: entity, insertInto: context)
        taskObject.taskName = taskName
        taskObject.taskDeadline = taskDeadline
        
        do {
            try context.save()
            taskArray.append(taskObject)
            taskArray = taskArray.sorted { $0.taskName! < $1.taskName! }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func loadFromCoredata(_ taskArray: inout [TaskCore]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<TaskCore> = TaskCore.fetchRequest()
        
        do {
            taskArray = try context.fetch(fetchRequest)
        } catch let error as NSError{
            print(error)
        }
    }
    
    // MARK: - Weather UserDefaults
    
    private let kCityKey = "Persistance.kCityKey"
    private let kWeatherKey = "Persistance.kWeatherKey"
    private let kTempKey = "Persistance.kTempKey"
    private let kWindKey = "Persistance.kWindKey"
    
    var city: String? {
        set { UserDefaults.standard.set(newValue, forKey: kCityKey)}
        get { return UserDefaults.standard.string(forKey: kCityKey)}
    }
    
    var weather: String? {
        set { UserDefaults.standard.set(newValue, forKey: kWeatherKey)}
        get { return UserDefaults.standard.string(forKey: kWeatherKey)}
    }
    
    var temp: String? {
        set { UserDefaults.standard.set(newValue, forKey: kTempKey)}
        get { return UserDefaults.standard.string(forKey: kTempKey)}
    }
    
    var wind: String? {
        set { UserDefaults.standard.set(newValue, forKey: kWindKey)}
        get { return UserDefaults.standard.string(forKey: kWindKey)}
    }
    
}
