//
//  WeatherViewController.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 12.04.2021.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    
    var currentWeather: CurrentWeather?
    let apiClient: ApiClient = ApiClientImpl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityLabel.text = Persistance.shared.city
        weatherLabel.text = Persistance.shared.weather
        tempLabel.text = Persistance.shared.temp
        windLabel.text =  Persistance.shared.wind
    }
    
    @IBAction func updateWeather(_ sender: Any) {
        loadData()
    }
    
    
    func loadData(){
        apiClient.getCurrentWeather(completion: { result in
            DispatchQueue.main.async {
                self.currentWeather = result
                
                self.cityLabel.text = self.currentWeather?.name
                Persistance.shared.city = self.currentWeather?.name
                
                self.weatherLabel.text = self.currentWeather?.weather[0].main
                Persistance.shared.weather = self.currentWeather?.weather[0].main
                
                let tempCelcius: Double = (self.currentWeather?.main.temp ?? 0) - 273.15
                self.tempLabel.text = String(format: "%.1f", tempCelcius) + "с"
                Persistance.shared.temp = self.tempLabel.text
                
                self.windLabel.text = String(self.currentWeather?.wind.speed ?? 0) + "м/с"
                Persistance.shared.wind = self.windLabel.text
            }
        })
    }

}
