//
//  ApiClient.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 12.04.2021.
//

import Foundation
import Alamofire

protocol ApiClient {
    func getCurrentWeather(completion: @escaping (CurrentWeather) -> Void)
}

class ApiClientImpl: ApiClient {
    func getCurrentWeather(completion: @escaping (CurrentWeather) -> Void) {
        let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=33e8d5442f02a9d804bf9d5b796505d1")!
        AF.request(url).responseData {response in
            if let data = response.value {
                let currentWeather: CurrentWeather = try! JSONDecoder().decode(CurrentWeather.self, from: data )
                completion(currentWeather)
            }
        }
    }
}
