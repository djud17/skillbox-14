//
//  ToDoViewController.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 10.04.2021.
//

import UIKit

protocol addingTaskDelegate {
    func addingTask(_ task: Task)
}

class ToDoViewController: UIViewController {

    @IBOutlet weak var doListTableView: UITableView!
    
    var taskArray: [Task] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Persistance.shared.realmRead(&taskArray)
        taskArray = taskArray.sorted { $0.taskName < $1.taskName }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? addTaskViewController, segue.identifier  == "addTask" {
            vc.delegate = self
        }
    }
}

extension ToDoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath)
        let task = taskArray[indexPath.row]
        
        cell.textLabel?.text = task.taskName
        cell.detailTextLabel?.text = task.taskDeadline
        
        return cell
    }
    
}

extension ToDoViewController: addingTaskDelegate{
    func addingTask(_ task: Task) {
        
        Persistance.shared.realmWrite(task)
        
        taskArray.append(task)
        taskArray = taskArray.sorted { $0.taskName < $1.taskName }
        
        doListTableView.reloadData()
    }
}

