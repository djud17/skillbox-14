//
//  CurrentWeatherStruct.swift
//  skillbox_14
//
//  Created by Давид Тоноян  on 12.04.2021.
//

import Foundation

// MARK: - CurrentWeather
struct CurrentWeather: Codable {
    let weather: [Weather]
    let main: Main
    let wind: Wind
    let name: String
}

// MARK: - Main
struct Main: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure: Int

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
    }
}

// MARK: - Weather
struct Weather: Codable {
    let main, weatherDescription: String

    enum CodingKeys: String, CodingKey {
        case main
        case weatherDescription = "description"
    }
}

// MARK: - Wind
struct Wind: Codable {
    let speed, deg: Int
}

// MARK: - fiveDaysWeather
struct FiveDaysWeather: Codable {
    let list: [List]
    let city: City
}

// MARK: - City
struct City: Codable {
    let name: String
}

// MARK: - List
struct List: Codable {
    let main: MainClass
    let weather: [Weather]
    let wind: Windy
    let dtTxt: String
    
    enum CodingKeys: String, CodingKey {
        case main,weather,wind
        case dtTxt = "dt_txt"
    }
}

// MARK: - MainClass
struct MainClass: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, seaLevel, grndLevel, humidity: Int

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
    }
}

// MARK: - Windy
struct Windy: Codable {
    let speed: Double
    let deg: Int
}
